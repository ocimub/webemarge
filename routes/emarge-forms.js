const express = require('express');
const router = express.Router();
const bu = require('../utils/buffer-utils');
const Event = require('../models/event');

router.get('/:eventParams', async (req, res, next) => {

    try {
        let encodedParams = req.params.eventParams;
        let params = bu.decode(encodedParams);

        let data = await JSON.parse(params);

        let event = new Event(data.title, data.date);
        event.setSessions(data.sessions);
        event.setEndDate(data.endDate);

        if (event.isOutdated()) {
            res.render('expired', {
                message: "Ce formulaire a expiré et n'est plus disponible"
            });
        } else {
            res.render('emarge-form', {
                data: encodedParams,
                title: event.title,
                date: event.date.toISOString().slice(0, 10),
                sessions: event.sessions
            });
        }
    } catch (e) {
        next(e);
    }
});

module.exports = router;