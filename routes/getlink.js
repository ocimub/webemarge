const express = require('express');
const router = express.Router();
const multer = require('multer');

const bu = require('../utils/buffer-utils');
const {
  asyncHandler
} = require('../utils/async-middleware');
const Event = require('../models/event');

const upload = multer();

router.get('/', (req, res, next) => {
  res.render('getlink');
});

router.post('/', upload.none(), asyncHandler(async (req, res, next) => {
  let event = new Event(req.body.title, req.body.date);
  event.setSessions(req.body.sessions);
  event.setEndDate(req.body.enddate);
  
  let params = bu.encode(event);
  let html = req.protocol + '://' + req.get('Host') + '/f/' + params;
  res.send(html);
}));

module.exports = router;