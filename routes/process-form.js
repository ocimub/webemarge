const express = require('express');
const router = express.Router();
const {
    asyncHandler
} = require('../utils/async-middleware');
const bu = require('../utils/buffer-utils');
const EmargeService = require('../utils/emarge-service');
const Emargement = require('../models/emargement');
const Event = require('../models/event');

router.post('/', asyncHandler(async (req, res, next) => {

    //décodage des données de l'évènement
    const eventData = await JSON.parse(bu.decode(req.body.data));

    //Données de l'évènement
    const event = new Event(eventData.title, eventData.date);
    event.setSessions(eventData.sessions);

    //Données de l'émargement
    const emargement = new Emargement(event, req.body.nom, req.body.prenom, req.body['sessions[]'], req.body.signature);
    const emargeService = new EmargeService();
    const pdf = await emargeService.generatePdf(emargement);
    res.setHeader('Content-Type', 'application/pdf');
    res.send(pdf);
}));

module.exports = router;