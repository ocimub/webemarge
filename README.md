# Webemarge #

Webemarge est une application web Node.js d'émargement pour les évènements [Ocim](https://ocim.fr).

## Installation ##

* Le code a un dépôt public sur Bitbucket. `git clone https://lescaph@bitbucket.org/ocimub/webemarge.git`
* Installation des dépendances avec `npm install` ou `npm install --production` pour une installation sans les devDependencies

## Configuration ##

* Configurez les variables d'environnement
* Lancez l'application avec `npm run dev` en dévelopement ou `npm start` en production

### Variables d'environnement ###

* NODE_ENV : 'production' ou 'development'. Déjà configuré si vous utilisez les commandes npm.
* MAIL_ADMIN : la page d'erreur invite à écrire à cette adresse mail
* PORT : le port de l'application (défaut: 3000)

## Utilisation ##

* A l'adresse [http(s)://app_url:port/getlink], un premier formulaire permet de configurer un évènement et d'obtenir le lien vers son formulaire d'émargement.
* Ce deuxième formulaire permet de générer un pdf avec les informations d'émargement dont la signature.
