class Event {

    constructor(title, date) {
        this.title = title;
        this.date = this.#formatDate(date);
    }

    setSessions(sessions) {
        if (sessions) {
            this.sessions = sessions;
        }
    }

    setEndDate(date) {
        if (date) {
            let endDate = this.#formatDate(date);
            if (this.#datesCompare(this.date, endDate)) {
                this.endDate = endDate;
            } else {
                throw Error('La date de validité du formulaire doit être postérieure à la date de l\'évènement');
            }
        }
    }

    isOutdated() {
        if (this.endDate) {
            return this.#datesCompare(this.endDate, Date.now())
        } else {
            return false;
        }
    }

    dateToString() {
        const dateOptions = {
            weekday: 'long',
            year: 'numeric',
            month: 'long',
            day: 'numeric'
        };
        return this.date.toLocaleDateString('fr-FR', dateOptions);
    }

    #formatDate(date) {
        if (this.#isValidDate(date)) {
            return date;
        } else {
            let newDate = new Date(date);
            if (this.#isValidDate(newDate)) {
                return newDate;
            } else {
                throw Error('Le format de la date n\'est pas valide');
            }
        }
    }

    #datesCompare(firstDate, secondDate) {
        return firstDate < secondDate;
    }

    #isValidDate(d) {
        return d instanceof Date && !isNaN(d);
    }
}

module.exports = Event;