const Event = require('./event');

class Emargement {

    constructor(event, nom, prenom, sessions, signature) {
        this.event = this.#isEvent(event) ? event : null;
        this.nom = nom;
        this.prenom = prenom;
        this.date = new Date();
        this.sessions = sessions;
        this.signature = signature;
    }

    #isEvent(val) {
        let v = val instanceof Event;
        if (v) {
            return v;
        } else {
            throw Error( val + ' n\'est pas une instance de la classe Event');
        }
    }
}

module.exports = Emargement;