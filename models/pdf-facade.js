const PDFDocument = require('pdfkit');
const fs = require('fs');

const tempFileName = './temp/output.pdf';

module.exports = class PdfFacade {

    #textAndSignature(text, image) {
        return this.doc
            .text(text)
            .image(image, {
                scale: 0.70
            });
    }

    createDocument() {
        this.doc = new PDFDocument();
        this.doc.pipe(fs.createWriteStream(tempFileName));
        return this;
    }

    writeTitle(title) {
        this.doc.font('Helvetica-Bold');
        this.doc.text(title, {
            align: 'center'
        });
        this.doc.font('Helvetica');
        return this;
    }

    writeLabelValuePair(label, value) {
        if (value) {
            this.doc
                .font('Helvetica-Bold')
                .text(`${label} : `, {
                    continued: true
                })
                .font('Helvetica')
                .text(value);
        }
        return this;
    }

    writeText(text) {
        this.doc.text(text);
        return this;
    }

    writeLogo(pathToImage) {
        this.doc.image(pathToImage, {
            width: 100
        });
        return this;
    }

    writeSignature(image) {
        this.doc.image(image, {
            scale: 0.80
        });
        return this;
    }

    writeSessionsAndSignature(sessions, image) {
        console.log(sessions, image);
        this.doc.moveDown();
        if (sessions) {
            if (Array.isArray(sessions)) {
                let y = this.doc.y;
                for (let i = 0; i < sessions.length; i++) {
                    if (i % 2 == 0) {
                        this.doc.x = 72;
                        y = this.doc.y;
                    } else {
                        this.doc.x = 350;
                        this.doc.y = y;
                    }
                    this.#textAndSignature(sessions[i], image);
                }
            } else {
                this.#textAndSignature(sessions, image);
            }
        } else {
            this.#textAndSignature("- aucune session", image);
        }
        return this;
    }

    writeFooter(text) {
        this.doc
            .fontSize(10)
            .text(text, 20, this.doc.page.height - 50, {
                lineBreak: false
            });
        return this;
    }

    closeAndGetBytes() {
        return new Promise((resolve, reject) => {
            try {
                this.doc.end();
                setTimeout(() => {
                    resolve(fs.readFileSync(tempFileName))
                }, 250);
            } catch (err) {
                reject(err);
            }
        });
    }
}