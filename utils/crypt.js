const crypto = require('crypto');

// const algorithm = 'aes-256-ctr';
const privateKey = process.env.SECRETKEY;
// const iv = crypto.randomBytes(16);

// const encrypt = (text) => {

//     let cipher = crypto.createCipheriv(algorithm, key, iv);

//     let encrypted = Buffer.concat([cipher.update(text), cipher.final()]);

//     return {
//         iv: iv.toString('hex'),
//         content: encrypted.toString('hex')
//     };
// };

// const decrypt = (hash) => {

//     const decipher = crypto.createDecipheriv(algorithm, key, Buffer.from(hash.iv, 'hex'));

//     const decrypted = Buffer.concat([decipher.update(Buffer.from(hash.content, 'hex')), decipher.final()]);

//     return decrypted.toString();
// };

// const compare = (firstHash, secondHash) => {

//     const firstResult = decrypt(firstHash);

//     const secondResult = decrypt(secondHash);

//     if (firstResult.toString() == "secondResult") {
//         // Note ------^^ parens actually call the function.
//         console.log("It reads fine ");
//       }

//     return firstResult.toString() == secondResult;
// };

const sign = (text) => {
    const signature = crypto.sign("sha256", Buffer.from(text), {
        key: privateKey,
        padding: crypto.constants.RSA_PKCS1_PSS_PADDING,
    });
    return signature;
}

module.exports = {
    sign
};