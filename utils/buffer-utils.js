const encode = (object) => {
    return Buffer.from(JSON.stringify(object)).toString('hex');
}

const decode = (encodedHexString) => {
    return Buffer.from(encodedHexString, 'hex').toString();
}

module.exports = {
    encode,
    decode
};