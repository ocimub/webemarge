const {
    createLogger,
    format,
    transports
  } = require('winston');
  require('winston-daily-rotate-file');
  const {
    combine,
    timestamp,
    label,
    printf
  } = format;

  // logger transport file
const transportDailyFile = new transports.DailyRotateFile({
    filename: 'webemarge-%DATE%.log',
    dirname: 'logs',
    zippedArchive: true,
    maxSize: '10m',
    maxFiles: '14d',
    format: combine(
      timestamp({
        format: 'YYYY-MM-DD HH:mm:ss',
      }),
      printf(info => `${info.timestamp} ${info.level}: ${info.message}`)
    ),
    level: 'error'
  });
  
  const logger = createLogger({
    exitOnError: false,
    transports: [
      transportDailyFile
    ]
  });
  
  // si on n'est pas en production, logs vers la console uniquement
  if (process.env.NODE_ENV !== 'production') {
    logger.clear()
      .add(new transports.Console({
        level: 'debug',
        format: combine(
          format.colorize(),
          printf(info => `${info.level}: ${info.message}`)
        )
      }));
  }

module.exports = logger;