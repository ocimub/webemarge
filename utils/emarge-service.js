const PdfFacade = require('../models/pdf-facade');

module.exports = class EmargeService {
    generatePdf(emargement) {

        const event = emargement.event;

        const pdf = new PdfFacade;
        const dateOptions = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };

        return pdf.createDocument()
            .writeLogo('./public/images/logo.png')
            .writeTitle('Attestation de présence')
            .writeLabelValuePair('\nFormation', event.title)
            .writeLabelValuePair('Session', event.session)
            .writeLabelValuePair('Date de l\'évènement', event.dateToString())
            .writeText('\nJe soussigné ' + emargement.nom + ' ' + emargement.prenom + ' atteste avoir suivi les sessions suivantes de la formation :')
            .writeSessionsAndSignature(emargement.sessions, emargement.signature)
            .writeFooter('\nDocument généré le ' + emargement.date.toLocaleDateString('fr-FR', dateOptions) + ' à ' + emargement.date.getHours() + ':' + (emargement.date.getMinutes() < 10 ? '0' : '') + emargement.date.getMinutes())
            .closeAndGetBytes();
    }
}