module.exports = {
  //surround async/await with try/catch
  asyncHandler: fn => (req, res, next) => {
    return Promise
      .resolve(fn(req, res, next))
      .catch(next);
  }
};