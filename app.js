const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const morgan = require('morgan');

const logger = require('./utils/logger');
require('dotenv').config();

const emargeFormsRouter = require('./routes/emarge-forms');
const getlinkRouter = require('./routes/getlink');
const processFormsRouter = require('./routes/process-form');

const app = express();

// moteur de vue
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// logs http avec morgan en developpement
if (process.env.NODE_ENV === 'development') app.use(morgan('dev'));

app.use(express.json());
app.use(express.urlencoded({
  extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/getlink', getlinkRouter);
app.use('/f', emargeFormsRouter);
app.use('/process', processFormsRouter);

app.get('/favicon.ico', (req, res) => res.status(204).end());

// interception des erreurs 404
app.use((req, res, next) => {
  next(createError(404, "Cette page est introuvable."));
});

// gestion des erreurs
app.use((err, req, res, next) => {

  //err.status = 500 si null ou undefined
  err.status = err.status ?? 500;

  // log en cas d'erreur
  logger.error(`${err.status} - ${err.message} - ${req.originalUrl} - ${req.method} - ${req.ip}`);

  // message d'erreur original en développement et générique en production si err.status = 500
  res.locals.message = req.app.get('env') === 'production' && (err.status === 500) ? "Une erreur est survenue. Si le problème persiste, contactez l'administrateur " + process.env.MAIL_ADMIN : err.message;

  // objet erreur original en développement et vide en production
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // affichage de la page d'erreur
  res.status(err.status);
  res.render('error');
});

module.exports = app;