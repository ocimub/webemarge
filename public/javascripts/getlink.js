const copyLink = document.getElementById("copylink");
const form = document.getElementById("generate-link-form");
const displayError = document.getElementById("display-error");
const linkContent = document.getElementById("link");


window.addEventListener("load", function () {
    function sendData() {
        const XHR = new XMLHttpRequest();

        // Liez l'objet FormData et l'élément form
        const FD = new FormData(form);

        // Si succès on affiche le lien sinon on affiche l'erreur
        XHR.addEventListener("load", function (event) {
            if (event.target.status == "200") {
                displayError.innerHTML = "";
                linkContent.innerHTML = event.target.responseText;
            } else {
                linkContent.innerHTML = "";
                displayError.innerHTML = event.target.responseText;
            }
        });

        // Definissez ce qui se passe en cas d'erreur
        XHR.addEventListener("error", function (event) {
            displayError.innerHTML = event.target.responseText;
        });

        // Configurez la requête
        XHR.open("POST", "/getlink");

        // Les données envoyées sont ce que l'utilisateur a mis dans le formulaire
        XHR.send(FD);
    }
    //soumission du formulaire
    form.addEventListener("submit", function (event) {
        event.preventDefault();
        copyLink.classList.remove("d-none");
        sendData();
    });


    //création des groupes input pour les sessions
    let next = 1;
    const inputGroupSessions = document.getElementById("input-group-sessions");
    const sessionsBtnAdd = document.getElementById("btn-add-session");
    
    //clic sur le bouton d'ajout des sessions
    sessionsBtnAdd.addEventListener("click", (e) => {
        let inputGroup = createInputGroup(next);
        next++;
        inputGroupSessions.appendChild(inputGroup);
    });

    //délégation pour les boutons 'remove' créés dynamiquement
    document.addEventListener('click', function (e) {
        if (e.target && e.target.classList.contains('btn-remove-session')) {
            const id = e.target.getAttribute("data-id");
            document.getElementById("input-group-" + id).remove();
        }
    });

    //création d'un élément div.input-group avec input et button remove 
    function createInputGroup(id) {
        //création d'un élément input-group
        let inputGroup = document.createElement('div');
        inputGroup.id = "input-group-" + id;
        inputGroup.className = "input-group";

        //création d'un élément input
        let input = document.createElement('input');
        input.id = "sessions-input-" + id;
        input.type = "text";
        input.className = "form-control";
        input.setAttribute("aria-describedby", "sessions-help");
        input.name = "sessions[]";

        //création d'un élément button
        let button = document.createElement('button');
        button.dataset.id = id;
        button.type = "button";
        button.className = "btn btn-danger btn-remove-session";
        button.setAttribute("aria-label", "Retirer");
        button.innerHTML = "-";

        inputGroup.appendChild(input);
        inputGroup.appendChild(button);

        return inputGroup;
    }

    //clic sur le bouton de copie du texte
    copyLink.addEventListener("click", (e) => {
        const link = document.getElementById("link");

        link.focus();
        link.select();
        link.setSelectionRange(0, 99999); /* appareils mobiles */
        try {
            document.execCommand("copy");
        } catch (e) {
            alert("Erreur lors de la copie " + e);
        }
    });
});